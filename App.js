import React from 'react';
import { Provider } from 'react-redux'

import AppNavigator from './src/app/navigation'
import configureStore from './src/app/store'

const store = configureStore()

export default function App() {
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  )
}