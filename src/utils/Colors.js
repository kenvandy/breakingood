export default {
    primary: 'rgb(59, 59, 59)',
    black: '#000',
    white: '#fff',
    transparent: 'transparent',
    green: 'rgb(103, 173, 91)',
    gold: 'rgb(239, 169, 68)',
    blue: 'rgb(51, 118, 208)',
    gray: '#BFBFBF',
}