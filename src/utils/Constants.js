import { Dimensions, Platform } from 'react-native'

export const host = 'https://breakingbadapi.com'

export const baseApi = {
  host,
  apiRootPath: '/api',
  timeout: 1000 * 60,
}


export const isIphoneX = () => {
  const dimen = Dimensions.get('window')
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 || dimen.width === 812)
  )
}

export const isIphoneXsMax = () => {
  const dimen = Dimensions.get('window')
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 896 || dimen.width === 896)
  )
}


export default {
    storageKey: {
      comment: {
        COMMENT_LIST_STORAGE_KEY: 'comment:commentListStorageKey'
      }
    },
    layout: {
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    },
    baseApi,
    emailRegex: /^[^\s@]+@[^\s@]+\.[^\s@]+$/
  }