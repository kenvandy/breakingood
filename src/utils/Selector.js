import * as R from 'ramda'

const xSplit = properties => {
  return properties.split('.')
}

export const orArray = (properties, record) => {
  return R.pathOr([], xSplit(properties))(record)
}

export const orNull = (properties, record) => {
  return R.pathOr(null, xSplit(properties))(record)
}

export const orObject = (prototypes, record) => {
  return R.pathOr({}, xSplit(prototypes))(record)
}

export const orNumber = (prototypes, record) => {
  return R.pathOr(0, xSplit(prototypes))(record)
}
