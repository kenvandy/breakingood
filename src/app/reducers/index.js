import { combineReducers } from 'redux'

import HomeReducer from '../../modules/Home/reducer'

const reducer = combineReducers({
  home: HomeReducer,
})

export default reducer
