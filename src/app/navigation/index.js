import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { Routes } from '../../utils'
import { List, Detail } from '../../modules/Home'

const Stack = createStackNavigator()

function Navigator() {
  function HomeStack() {
    return (
      <Stack.Navigator>
        <Stack.Screen name={Routes.list} component={List} />
        <Stack.Screen name={Routes.detail} component={Detail} />
      </Stack.Navigator>
    )
  }

  return <NavigationContainer>{HomeStack()}</NavigationContainer>
}

export default Navigator
