import { useState, useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { throwError } from '../services/ErrorBoundary'

import * as QuoteApi from '../services/quoteApi'
import { orArray } from '../utils/Selector'

export const useRandomQuote = author => {
  const [data, setData] = useState([])
  const [isLoading, setLoading] = useState(false)
  const dispatch = useDispatch()


  const fetchQuote = useCallback(async () => {
    try {
      setLoading(true)
      const quote = await QuoteApi.fetchRandamQuotes(author)
      if (!quote.data.length) {
        console.log('NO DATA')
      }
      const quoteData = orArray('data', quote)
      setData(quoteData)
    } catch (error) {
      dispatch(
        throwError({ error, message: 'FETCH QUOTE ERROR' })
      )
    } finally {
      setLoading(false)
    }
  }, [data, isLoading])

  return {
    isLoading,
    data,
    setData,
    fetchQuote
  }
}
