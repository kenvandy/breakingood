import React, { useState, useEffect, useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { throwError } from '../services/ErrorBoundary'

import { Constants, Storage } from '../utils'
import { uniqueIdGenerator } from '../utils/Helpers'
import { orNull } from '../utils/Selector'

export const useComments = () => {
  const [data, setData] = useState([])
  const [isLoading, setLoading] = useState(false)
  const dispatch = useDispatch()

  const getCommentStorage = useCallback(
    async userData => {
      try {
        setLoading(true)
        const commentStorage = await Storage.get(
          Constants.storageKey.comment.COMMENT_LIST_STORAGE_KEY
        )
        if (commentStorage) {
          const dataFilter = commentStorage.filter(
            item => item.userId === userData.char_id
          )
          setData(dataFilter)
        }
      } catch (error) {
        dispatch(
          throwError({ error, message: 'GET COMMENTS ERROR' })
        )
      } finally {
        setLoading(false)
      }
    },
    [data]
  )

  const setCommentStorage = useCallback(
    async (userData, comment) => {
      try {
        const uniqueID = uniqueIdGenerator()
        let dataArr = []
        dataArr.push({
          id: uniqueID,
          userId: orNull('char_id', userData),
          comment,
          commentName: 'Ken',
          startAt: Date.now()
        })
        const commentStorage = await Storage.get(
          Constants.storageKey.comment.COMMENT_LIST_STORAGE_KEY
        )

        if (commentStorage) {
          const newData = commentStorage.concat(dataArr)
          dataArr = []
          await Storage.set(
            Constants.storageKey.comment.COMMENT_LIST_STORAGE_KEY,
            newData
          )
          getCommentStorage(userData)
        } else {
          await Storage.set(
            Constants.storageKey.comment.COMMENT_LIST_STORAGE_KEY,
            dataArr
          )
          getCommentStorage(userData)
        }
      } catch (error) {
        dispatch(
          throwError({ error, message: 'SET COMMENTS ERROR' })
        )
      }
    },
    [data]
  )

  return {
    data,
    getCommentStorage,
    setCommentStorage
  }
}
