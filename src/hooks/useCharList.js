import { useState, useCallback } from 'react'
import { useDispatch } from 'react-redux'
import * as CharacterAPI from '../services/characterApi'
import { throwError } from '../services/ErrorBoundary'
import { orNull } from '../utils/Selector'

export const useCharacterList = () => {
  const [data, setData] = useState([])
  const [isLast, setIsLast] = useState(false)
  const [isLoading, setLoading] = useState(false)
  const [page, setPage] = useState(1)
  const dispatch = useDispatch()

  const fetchCharacterList = useCallback(
    async options => {
      let _page = orNull('page', options)
      _page = _page !== null ? options.page : 1
      try {
        setLoading(true)
        const characters = await CharacterAPI.fetchCharacters(_page)
        if (!characters.data.length) {
          console.log('NO DATA')
        }
        if (_page > 1) {
          setData(data.concat(characters.data))
        } else {
          setData(characters.data)
          setIsLast(false)
        }
        setPage(_page)
      } catch (error) {
        if (error.message.includes('status code 400')) {
          setIsLast(true)
        } else {
          dispatch(
            throwError({ error, message: 'FETCH CHARACTER LIST ERROR' })
          )
        }
      } finally {
        setLoading(false)
      }
    },
    [data]
  )

  const fetchMore = useCallback(async () => {
    if (isLast) return
    await fetchCharacterList({ page: page + 1 })
  }, [fetchCharacterList, isLast, page])
  return {
    data,
    setData,
    fetchMore,
    fetchCharacterList,
    page,
    isLoading
  }
}
