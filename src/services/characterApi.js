import axios from 'axios'
import { baseApi } from '../utils/Constants'

const axiosInstance = axios.create({
    baseURL: baseApi.host + baseApi.apiRootPath,
    timeout: baseApi.timeout
})

export const fetchCharacters = async(page) => {
    return await axiosInstance.request({
        method: 'GET',
        url: '/characters',
        params: {
            limit: 10,
            offset: page
        }
    })
}