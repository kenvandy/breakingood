// THIS IS ALMOST COMPLETE SOUTION TO HANDLE, CAPTURE ERROR
// FOR ANY PURPOSES

import { createActions, handleActions } from 'redux-actions'
import { fromJS } from 'immutable'

import { Alert } from 'react-native'

const prefix = 'app'

// action type
const THROW_ERROR = 'THROW_ERROR'
const RESET_ERROR = 'RESET_ERROR'
const COMPLETE_INITIAL_LOADING = 'COMPLETE_INITIAL_LOADING'

export const initialState = fromJS({
  initialLoading: true,
  error: null,
})

export const throwError = (params) => (
  dispatch
) => {
  const { error, message } = params
  const _message = message

  dispatch(
    actions.throwError({
      error,
      message: _message,
    })
  )
  Alert.alert('Warning', message)
}

// action-creator
const noop = () => null
export const actions = createActions(
  {
    [THROW_ERROR]: (payload) => payload,
    [RESET_ERROR]: noop,
    [COMPLETE_INITIAL_LOADING]: noop,
  },
  {
    prefix,
  }
)

// reducer
const reducer = handleActions(
  {
    [THROW_ERROR]: (state,action) => {
      const error = fromJS({
        data: action.payload.error,
        message: action.payload.message,
      })
      return state.set('error', error)
    },
    [RESET_ERROR]: (state) => {
      return state.set('error', null)
    },
    [COMPLETE_INITIAL_LOADING]: (state) => {
      return state.set('initialLoading', false)
    },
  },
  initialState,
  {
    prefix,
  }
)

export default reducer
