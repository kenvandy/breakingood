import axios from 'axios'
import { baseApi } from '../utils/Constants'

const axiosInstance = axios.create({
    baseURL: baseApi.host + baseApi.apiRootPath,
    timeout: baseApi.timeout
})

export const fetchRandamQuotes = async(author) => {
    return await axiosInstance.request({
        method: 'GET',
        url: '/quote/random',
        params: {
            author
        }
    })
}