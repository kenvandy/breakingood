import styled from 'styled-components/native'
import { Colors } from '../../../utils'

const IMAGE_WH = 130

export const Container = styled.View`
  flex: 1;
  align-items: center;
  margin-top: 20px;
`

export const Image = styled.Image`
  width: ${IMAGE_WH}px;
  height: ${IMAGE_WH}px;
  border-radius: ${IMAGE_WH / 2}px;
`

export const UserName = styled.Text`
  font-size: 22px;
  font-weight: bold;
  text-align: center;
  margin-top: 10px;
`

export const NickName = styled.Text`
  font-size: 18px;
  font-style: italic;
  margin-top: 5px;
  text-align: center;
`

export const DetailWrapper = styled.View`
  flex: 1;
  width: 100%;
` 

export const RowWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-width: 0.5px;
  border-bottom-color: ${Colors.primary};
  margin-bottom: 25px;
  padding-bottom: 10px;
  margin-horizontal: 15px;
`

export const LeftText = styled.Text`
  font-size: 18px;
  font-weight: bold;
`

export const RightText = styled.Text`
  font-size: 18px;
  width: 60%;
  text-align: right;
`

export const QuoteWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
  margin-top: 10px;
  margin-horizontal: 15px;
`

export const Quote = styled.Text`
  font-size: 16px;
  font-style: italic;
  font-weight: bold;
  color: ${Colors.gold};
  width: ${props => props.length ? '65%' : '40%'};
`

export const GetQuoteWrapper = styled.TouchableOpacity`
  background-color: ${Colors.green};
  padding: 5px;
  border-radius: 3px;
  justify-content: center;
  align-items: center;
  margin-left: 10px;
`

export const GetQuote = styled.Text`
  color: ${Colors.white};
`

export const SeeCommentWrapper = styled.TouchableOpacity`
  width: 100%;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 60px;
`

export const SeeComment = styled.Text`
  text-decoration: underline;
  text-decoration-color: ${Colors.blue};
  font-size: 16px;
  color: ${Colors.blue};
`