import React, { useCallback, useEffect, useState } from 'react'
import { ActivityIndicator } from 'react-native'
import { useRoute } from '@react-navigation/native'
import { orArray, orNull } from '../../../utils/Selector'

import {
  Container,
  Image,
  UserName,
  NickName,
  DetailWrapper,
  RowWrapper,
  LeftText,
  RightText,
  QuoteWrapper,
  Quote,
  GetQuoteWrapper,
  GetQuote,
  SeeCommentWrapper,
  SeeComment
} from './styled'
import { useRandomQuote } from '../../../hooks/useQuotes'
import Comment from '../Comments'

function Home() {
  const [isShowModal, setShowModal] = useState(false)

  const route = useRoute()

  const userData = orNull('params.data', route)
  const { data, fetchQuote, isLoading } = useRandomQuote(userData.name)
  useEffect(() => {
    fetchQuote()
  }, [])

  const Row = (left, right) => {
    return (
      <RowWrapper>
        <LeftText>{left}</LeftText>
        <RightText>{right}</RightText>
      </RowWrapper>
    )
  }

  const renderCharDetail = useCallback(() => {
    const { status } = userData
    const appearances = orArray('appearance', userData)
    const occupations = orArray('occupation', userData)
    return (
      <DetailWrapper>
        {Row('Id', orNull('char_id', userData))}
        {Row('Appearance', appearances.length && appearances.join(', '))}
        {Row('Birthday', orNull('birthday', userData))}
        {Row('Category', orNull('category', userData))}
        {Row('Occupation', occupations.length && occupations.join(', '))}
        {Row('Portrayed', orNull('portrayed', userData))}
        {Row('Status', status)}
      </DetailWrapper>
    )
  }, [userData])

  const renderRandomQuotes = useCallback(() => {
    if (!isLoading) {
      return (
        <QuoteWrapper>
          <Quote length={data.length}>"{data.length ? data[0].quote : "I am thinking..."}"</Quote>
          <GetQuoteWrapper onPress={() => fetchQuote()}>
            <GetQuote>Hear more</GetQuote>
          </GetQuoteWrapper>
        </QuoteWrapper>
      )
    }
    return <ActivityIndicator size={'small'} /> 
  }, [data, isLoading])

  const renderSeeComment = () => {
      return (
        <SeeCommentWrapper onPress={() => setShowModal(true)}>
          <SeeComment>See Comments</SeeComment>
        </SeeCommentWrapper>
      )
  }

  return (
    <Container>
      <Image source={{ uri: userData.img }} />
      <UserName>{orNull('name', userData)}</UserName>
      <NickName>{orNull('nickname', userData)}</NickName>
      {renderRandomQuotes()}
      {renderCharDetail()}
      {renderSeeComment()}
      <Comment
        isShowModal={isShowModal}
        setShowModal={setShowModal}
        userData={userData}
      />
    </Container>
  )
}

export default Home
