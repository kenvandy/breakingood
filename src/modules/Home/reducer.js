import { handleActions } from 'redux-actions'

import {
    LOADING,
    setLoading,
} from './action-types'

import Model from './model'

const initialState = Model(null)


// THIS FUNCTION JUST DEMONSTRATES THAT REDUX WORKS
export const fireRedux = () =>  dispatch => {
   dispatch(setLoading(true))
}

const actions = {
    [LOADING]: (state, action) => state.setLoading(action.payload),
}


export default handleActions(actions, initialState)
