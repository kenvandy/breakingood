import { createAction } from 'redux-actions'

export const LOADING = 'CharList/LOADING'
export const setLoading = createAction(LOADING)
