import React, { useEffect } from 'react'
import { ActivityIndicator } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import { useCharacterList } from '../../../hooks/useCharList'
import {
  Container,
  FlatList,
  ItemWrapper,
  UserNameWrapper,
  Image,
  UserName,
  NickName,
  styles
} from './styled'
import { Routes } from '../../../utils'
import { fireRedux } from '../reducer'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

function keyExtractor(index) {
  return `CharacterListKey--->${index}`
}

function List(props) {
  const { fireRedux } = props
  const { data, fetchCharacterList, fetchMore, page, isLoading } = useCharacterList()
  const navigation = useNavigation()

  useEffect(() => {
    fireRedux()
    fetchCharacterList()
  }, [])

  const renderItem = ({ item }) => {
    const { img, name, nickname } = item
    return (
      <ItemWrapper
        onPress={() => navigation.navigate(Routes.detail, { data: item })}>
        <Image source={{ uri: img }} resizeMode={'contain'} />
        <UserNameWrapper>
          <UserName>{name}</UserName>
          <NickName>{nickname}</NickName>
        </UserNameWrapper>
      </ItemWrapper>
    )
  }

  const renderCharacterList = () => {
    return (
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(_item, index) => keyExtractor(index)}
        showsVerticalScrollIndicator={false}
        numColumns={2}
        horizontal={false}
        contentContainerStyle={styles.flatList}
        onEndReached={fetchMore}
        onEndReachedThreshold={0.45}
        ListHeaderComponent={() => {
          if (isLoading && page === 1) {
            return <ActivityIndicator />
          } 
          return null
        }}
      />
    )
  }

  return <Container>{renderCharacterList()}</Container>
}


const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fireRedux
    },
    dispatch
  )
}

export default connect(
  null,
  mapDispatchToProps
)(List)
