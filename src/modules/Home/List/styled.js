import { StyleSheet } from 'react-native'
import styled from 'styled-components/native'
import { Colors, Constants } from '../../../utils'

export const Container = styled.View`
  flex: 1;
  background-color: ${Colors.primary};
`

export const FlatList = styled.FlatList`
  flex: 1;
  padding-top: 20px;
`

export const ItemWrapper = styled.Pressable`
`

export const Image = styled.Image`
  width: ${Constants.layout.screenWidth / 2}px;
  height: 200px;
`

export const UserNameWrapper = styled.View`
  margin-bottom: 20px;
  margin-top: 5px;
  width: 100%;
  align-items: center;
`

export const UserName = styled.Text`
  font-size: 18px;
  color: ${Colors.white};
  
`
export const NickName = styled.Text`
  font-size: 14px;
  font-style: italic;
  color: ${Colors.white};
`

export const styles = StyleSheet.create({
  flatList: {
    paddingBottom: 30
  },
})