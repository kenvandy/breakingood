import { pathOr, construct, assocPath } from 'ramda'

function Home(record) {
    this.isLoading = pathOr(false, ['isLoading'], record)
    this.charData = pathOr(null, ['charData'], record)
}

Home.prototype = {
    setLoading: function(value) {
        return assocPath(['isLoading'], value, this)
    }
}

export default construct(Home)
