import React from 'react'
import { AntDesign } from '@expo/vector-icons'; 

import Modal from 'react-native-modal'
import { Container, Header, HeaderWrapper, styles } from './styled'
import CommentList from './List'
import { Colors } from '../../../utils';

function Comment(props) {
  const { isShowModal, userData, setShowModal } = props
  return (
    <Container>
      <Modal
        isVisible={isShowModal}
        coverScreen
        useNativeDriver
        style={styles.modal}
      >
        <HeaderWrapper>
          <Header />
          <Header>Comments</Header>
          <AntDesign
            name='close'
            size={24}
            color={Colors.black}
            onPress={() => setShowModal(false)}
          />
        </HeaderWrapper>

        <CommentList userData={userData} />
      </Modal>
    </Container>
  )
}

export default Comment