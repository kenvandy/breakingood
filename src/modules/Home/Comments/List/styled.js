import { Platform, StyleSheet } from 'react-native'
import styled from 'styled-components/native'
import { Colors } from '../../../../utils'


export const Container = styled.View`
  flex: 1;
  background-color: ${Colors.white};
`

export const TextInputWrapper = styled.View`
  width: 100%;  
  flex-direction: ${Platform.OS === 'ios' ? 'column' : 'row'};
  justify-content: space-between;
  background-color: ${Colors.white};
  border-top-color: ${Colors.gray};
  border-top-width: ${Platform.OS === 'android' ? '0.5px' : 0};
  padding-top: 5px;
  
`

export const TextInput = styled.TextInput`
  width: ${Platform.OS === 'ios' ? '100%' : '80%'};
  font-size: 14px;
  background-color: ${Colors.white};
  height: 40px;
  padding-left: 20px;
  padding-bottom: 5px;
  padding-right: ${Platform.OS === 'ios' ? '80px' : 0};
  border-top-color: ${Colors.gray};
  border-top-width: ${Platform.OS === 'ios' ? '0.5px' : 0};
`

export const SendBtn = styled.TouchableOpacity`
  background-color: ${Colors.green};
  padding-horizontal: 10px;
  padding-vertical: 5px;
  border-radius: 3px;
  justify-content: center;
  align-items: center;
  right: 10px;
  margin-bottom: 5px;
`

export const Send = styled.Text`
  color: ${Colors.white};
  font-size: 16px;
`
export const FlatList = styled.FlatList`
  flex: 1;
  padding-top: 20px;
`

export const ItemWrapper = styled.View`
  margin-horizontal: 20px;
  margin-bottom: 10px;
  padding-bottom: 5px;
  border-bottom-width: 0.5px;
  border-bottom-color: ${Colors.gray};
`


export const CommentName = styled.Text`
  font-size: 16px;
  font-weight: bold;
`

export const CommentText = styled.Text`

`

export const CommentTime = styled.Text`
  font-style: italic;
  margin-top: 5px;
`

export const styles = StyleSheet.create({
  flatList: {
    paddingBottom: 50
  },
})