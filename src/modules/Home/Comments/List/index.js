import React, { useEffect, useState, useCallback, useRef } from 'react'
import { Keyboard, Platform } from 'react-native'
import dayjs from 'dayjs'

import { useComments } from '../../../../hooks/useComments'
import { Colors } from '../../../../utils'
import {
  Container,
  FlatList,
  ItemWrapper,
  CommentName,
  CommentText,
  CommentTime,
  TextInputWrapper,
  TextInput,
  SendBtn,
  Send,
  styles
} from './styled'

function keyExtractor(index) {
  return `CommentListKey--->${index}`
}

function List(props) {
  const { userData } = props
  const { data, getCommentStorage, setCommentStorage } = useComments()
  const [commentText, setComment] = useState(null)

  const [keyboardOffset, setKeyboardOffset] = useState(30)
  const onKeyboardShow = event => setKeyboardOffset(event.endCoordinates.height)
  const onKeyboardHide = () => setKeyboardOffset(30)
  const keyboardDidShowListener = useRef()
  const keyboardDidHideListener = useRef()

  useEffect(() => {
    keyboardDidShowListener.current = Keyboard.addListener(
      'keyboardWillShow',
      onKeyboardShow
    )
    keyboardDidHideListener.current = Keyboard.addListener(
      'keyboardWillHide',
      onKeyboardHide
    )

    return () => {
      keyboardDidShowListener.current.remove()
      keyboardDidHideListener.current.remove()
    }
  }, [])

  useEffect(() => {
    getCommentStorage(userData)
  }, [])

  const renderItem = ({ item }) => {
    const { comment, commentName, startAt } = item
    const date = new Date(startAt)
    return (
      <ItemWrapper>
        <CommentName>{commentName}</CommentName>
        <CommentText>{comment}</CommentText>
        <CommentTime>{dayjs(date).format('DD/MM/YYYY HH:mm')}</CommentTime>
      </ItemWrapper>
    )
  }

  const renderCommentList = useCallback(() => {
    return (
      <FlatList
        data={data}
        extraData={data}
        renderItem={renderItem}
        keyExtractor={(_item, index) => keyExtractor(index)}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flatList}
      />
    )
  }, [data])

  const renderInput = () => {
    const keyboardPlatform = {
      position: Platform.OS === 'ios' ? 'absolute' : 'relative',
      bottom: Platform.OS === 'ios' ? keyboardOffset : null
    }
    return (
      <TextInputWrapper>
        <TextInput
          underlineColorAndroid={'transparent'}
          onChangeText={txt => setComment(txt)}
          placeholderTextColor={Colors.gray}
          placeholder='Add a comment...'
          value={commentText}
          style={keyboardPlatform}
          multiline
        />
        <SendBtn
          onPress={() => {
            setCommentStorage(userData, commentText)
            setComment(null)
          }}
          style={keyboardPlatform}
        >
          <Send>Send</Send>
        </SendBtn>
      </TextInputWrapper>
    )
  }

  return (
    <Container>
      {renderCommentList()}
      {renderInput()}
    </Container>
  )
}

export default List
