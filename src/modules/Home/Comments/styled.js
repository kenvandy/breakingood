import { StyleSheet } from 'react-native'
import styled from 'styled-components/native'
import { Colors } from '../../../utils'
import { isIphoneX, isIphoneXsMax } from '../../../utils/Constants'


export const Container = styled.View`
  flex: 1;
`

export const HeaderWrapper = styled.View`
  width: 100%;
  margin-top: ${isIphoneX() || isIphoneXsMax() ? '50px' : '20px'};
  padding-right: 5px;
  flex-direction: row;
  justify-content: space-between;
  border-bottom-color: ${Colors.gray};
  border-bottom-width: 0.5px;
  padding-bottom: 10px;
`

export const Header = styled.Text`
  text-align: center;
  font-size: 18px;
  font-weight: bold;
`

export const styles = StyleSheet.create({
  modal: {
    margin: 0,
    backgroundColor: Colors.white
  }
})