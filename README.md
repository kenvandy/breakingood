# BreakingGood

Build a Breaking Good character list application
Breaking Good is the name of a Radio show

## How to use
1. Install
- run ```yarn install``` or ```npm install``` to install dependencies
- cd ios && pod install

2. Run Project

 2.1. Expo Client tool
- Download Expo App to your iOS or Android:
https://expo.io/tools#client

- Then open terminal, run ```expo start``` 
- After that, using the Expo app to scan the QR code to run the BREAKINGOOD instantly

 2.2. XCode or Android Studio
Feel free to run this project via Xcode or Anroid Studio
